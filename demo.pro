#-------------------------------------------------
#
# Project created by QtCreator 2016-11-19T10:58:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = demo
TEMPLATE = app


SOURCES += main.cpp\
    datatables.cpp \
    mainwindow.cpp \
    menuwindow.cpp \
    onoff.cpp \
    qwintspindelegate.cpp \
    toptools.cpp

HEADERS  += \
    datatables.h \
    mainwindow.h \
    menuwindow.h \
    onoff.h \
    qwintspindelegate.h \
    toptools.h

FORMS    += \
    datatables.ui \
    mainwindow.ui \
    menuwindow.ui \
    onoff.ui \
    toptools.ui

RESOURCES += \
    res.qrc
