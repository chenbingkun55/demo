#ifndef MENUWINDOW_H
#define MENUWINDOW_H

#include <QFrame>

namespace Ui {
class MenuWindow;
}

class MenuWindow : public QFrame
{
    Q_OBJECT

public:
    explicit MenuWindow(QWidget *parent = nullptr);
    ~MenuWindow();

public slots:
    void on_btnCreateRule_clicked();

public:
    void ShowNullMsg();

private:
    Ui::MenuWindow *ui;
};

#endif // MENUWINDOW_H
