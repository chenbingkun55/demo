#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFrame>

namespace Ui {
class MainWindow;
}

class MainWindow : public QFrame
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
