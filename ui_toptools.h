/********************************************************************************
** Form generated from reading UI file 'toptools.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TOPTOOLS_H
#define UI_TOPTOOLS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TopTools
{
public:
    QWidget *gridLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnTack;
    QPushButton *btnNull5;
    QPushButton *btnNull4;
    QPushButton *btnNull3;
    QPushButton *btnNull1;
    QPushButton *btnNull2;
    QPushButton *btnNull6;
    QPushButton *btnNull7;
    QPushButton *btnNull8;
    QPushButton *btnExit;

    void setupUi(QWidget *TopTools)
    {
        if (TopTools->objectName().isEmpty())
            TopTools->setObjectName(QString::fromUtf8("TopTools"));
        TopTools->resize(1000, 60);
        TopTools->setStyleSheet(QString::fromUtf8("#TopTools\n"
"{\n"
"	\n"
"	background-color: rgb(240, 240, 240);\n"
"}"));
        gridLayoutWidget = new QWidget(TopTools);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(197, 0, 676, 60));
        horizontalLayout = new QHBoxLayout(gridLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetNoConstraint);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        btnTack = new QPushButton(gridLayoutWidget);
        btnTack->setObjectName(QString::fromUtf8("btnTack"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/icons/tack.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnTack->setIcon(icon);
        btnTack->setIconSize(QSize(50, 50));

        horizontalLayout->addWidget(btnTack);

        btnNull5 = new QPushButton(gridLayoutWidget);
        btnNull5->setObjectName(QString::fromUtf8("btnNull5"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/icons/null.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnNull5->setIcon(icon1);
        btnNull5->setIconSize(QSize(50, 50));

        horizontalLayout->addWidget(btnNull5);

        btnNull4 = new QPushButton(gridLayoutWidget);
        btnNull4->setObjectName(QString::fromUtf8("btnNull4"));
        btnNull4->setIcon(icon1);
        btnNull4->setIconSize(QSize(50, 50));

        horizontalLayout->addWidget(btnNull4);

        btnNull3 = new QPushButton(gridLayoutWidget);
        btnNull3->setObjectName(QString::fromUtf8("btnNull3"));
        btnNull3->setIcon(icon1);
        btnNull3->setIconSize(QSize(50, 50));

        horizontalLayout->addWidget(btnNull3);

        btnNull1 = new QPushButton(gridLayoutWidget);
        btnNull1->setObjectName(QString::fromUtf8("btnNull1"));
        btnNull1->setIcon(icon1);
        btnNull1->setIconSize(QSize(50, 50));

        horizontalLayout->addWidget(btnNull1);

        btnNull2 = new QPushButton(gridLayoutWidget);
        btnNull2->setObjectName(QString::fromUtf8("btnNull2"));
        btnNull2->setIcon(icon1);
        btnNull2->setIconSize(QSize(50, 50));

        horizontalLayout->addWidget(btnNull2);

        btnNull6 = new QPushButton(gridLayoutWidget);
        btnNull6->setObjectName(QString::fromUtf8("btnNull6"));
        btnNull6->setIcon(icon1);
        btnNull6->setIconSize(QSize(50, 50));

        horizontalLayout->addWidget(btnNull6);

        btnNull7 = new QPushButton(gridLayoutWidget);
        btnNull7->setObjectName(QString::fromUtf8("btnNull7"));
        btnNull7->setIcon(icon1);
        btnNull7->setIconSize(QSize(50, 50));

        horizontalLayout->addWidget(btnNull7);

        btnNull8 = new QPushButton(gridLayoutWidget);
        btnNull8->setObjectName(QString::fromUtf8("btnNull8"));
        btnNull8->setIcon(icon1);
        btnNull8->setIconSize(QSize(50, 50));

        horizontalLayout->addWidget(btnNull8);

        btnExit = new QPushButton(gridLayoutWidget);
        btnExit->setObjectName(QString::fromUtf8("btnExit"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/icons/shudown.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnExit->setIcon(icon2);
        btnExit->setIconSize(QSize(50, 50));

        horizontalLayout->addWidget(btnExit);


        retranslateUi(TopTools);

        QMetaObject::connectSlotsByName(TopTools);
    } // setupUi

    void retranslateUi(QWidget *TopTools)
    {
        TopTools->setWindowTitle(QCoreApplication::translate("TopTools", "Form", nullptr));
        btnTack->setText(QString());
        btnNull5->setText(QString());
        btnNull4->setText(QString());
        btnNull3->setText(QString());
        btnNull1->setText(QString());
        btnNull2->setText(QString());
        btnNull6->setText(QString());
        btnNull7->setText(QString());
        btnNull8->setText(QString());
        btnExit->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class TopTools: public Ui_TopTools {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TOPTOOLS_H
