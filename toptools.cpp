#include "toptools.h"
#include "ui_toptools.h"
#include "QCloseEvent"
#include "QMessageBox"
#include <iostream>
#include <string>

TopTools::TopTools(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::TopTools)
{
    ui->setupUi(this);
}

TopTools::~TopTools()
{
    delete ui;
}

void TopTools::ShowNullMsg(int num)
{
    QString strTitle = "提示";
    QString strText =  "功能" + QString::number(num) + "未实现";
    QMessageBox::information(this, strTitle, strText, QMessageBox::Ok, QMessageBox::NoButton);
}

void TopTools::on_btnTack_clicked()
{
    std::cout << "btn Tack." << std::endl;
    hide();
}

void TopTools::on_btnExit_clicked()
{
    std::cout << "btn Exit." << std::endl;

    QCoreApplication::exit(0);
}

void TopTools::on_btnNull1_clicked()
{
    std::cout << "btn Null1." << std::endl;

    ShowNullMsg(1);
}

void TopTools::on_btnNull2_clicked()
{
    std::cout << "btn Null2." << std::endl;

    ShowNullMsg(2);
}

void TopTools::on_btnNull3_clicked()
{
    std::cout << "btn Null3." << std::endl;

    ShowNullMsg(3);
}

void TopTools::on_btnNull4_clicked()
{
    std::cout << "btn Null4." << std::endl;

    ShowNullMsg(4);
}

void TopTools::on_btnNull5_clicked()
{
    std::cout << "btn Null5." << std::endl;

    ShowNullMsg(5);
}

void TopTools::on_btnNull6_clicked()
{
    std::cout << "btn Null6." << std::endl;

    ShowNullMsg(6);
}

void TopTools::on_btnNull7_clicked()
{
    std::cout << "btn Null7." << std::endl;

    ShowNullMsg(7);
}

void TopTools::on_btnNull8_clicked()
{
    std::cout << "btn Null8." << std::endl;

    ShowNullMsg(8);
}

