#ifndef TOPTOOLS_H
#define TOPTOOLS_H

#include <QFrame>

namespace Ui {
class TopTools;
}

class TopTools : public QFrame
{
    Q_OBJECT

public:
    explicit TopTools(QWidget *parent = nullptr);
    ~TopTools();

public slots:
    void on_btnTack_clicked();
    void on_btnExit_clicked();

    void on_btnNull1_clicked();
    void on_btnNull2_clicked();
    void on_btnNull3_clicked();
    void on_btnNull4_clicked();
    void on_btnNull5_clicked();
    void on_btnNull6_clicked();
    void on_btnNull7_clicked();
    void on_btnNull8_clicked();

private:
    void ShowNullMsg(int num);

private:
    Ui::TopTools *ui;
};

#endif // TOPTOOLS_H
