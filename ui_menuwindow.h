/********************************************************************************
** Form generated from reading UI file 'menuwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MENUWINDOW_H
#define UI_MENUWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include <onoff.h>

QT_BEGIN_NAMESPACE

class Ui_MenuWindow
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *btnTack;
    QComboBox *comboBox;
    QLineEdit *lineEdit;
    QLabel *label;
    QLabel *label_2;
    QPushButton *btnCreateRule;
    OnOff *onoff;

    void setupUi(QFrame *MenuWindow)
    {
        if (MenuWindow->objectName().isEmpty())
            MenuWindow->setObjectName(QString::fromUtf8("MenuWindow"));
        MenuWindow->resize(242, 600);
        MenuWindow->setStyleSheet(QString::fromUtf8("#MenuWindow\n"
"{\n"
"	\n"
"	background-color: rgb(240, 240, 240);\n"
"}"));
        horizontalLayoutWidget = new QWidget(MenuWindow);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(0, 50, 242, 82));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_3 = new QPushButton(horizontalLayoutWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(80, 80));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/icons/rules.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_3->setIcon(icon);
        pushButton_3->setIconSize(QSize(80, 80));

        horizontalLayout->addWidget(pushButton_3);

        pushButton_4 = new QPushButton(horizontalLayoutWidget);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setMinimumSize(QSize(80, 80));
        pushButton_4->setAutoFillBackground(false);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/icons/camera.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_4->setIcon(icon1);
        pushButton_4->setIconSize(QSize(80, 80));

        horizontalLayout->addWidget(pushButton_4);

        pushButton_5 = new QPushButton(horizontalLayoutWidget);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setMinimumSize(QSize(80, 80));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/icons/wifi.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_5->setIcon(icon2);
        pushButton_5->setIconSize(QSize(80, 80));

        horizontalLayout->addWidget(pushButton_5);

        btnTack = new QPushButton(MenuWindow);
        btnTack->setObjectName(QString::fromUtf8("btnTack"));
        btnTack->setGeometry(QRect(10, 10, 31, 31));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btnTack->sizePolicy().hasHeightForWidth());
        btnTack->setSizePolicy(sizePolicy);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/icons/tack_small.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnTack->setIcon(icon3);
        btnTack->setIconSize(QSize(50, 50));
        comboBox = new QComboBox(MenuWindow);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(170, 10, 61, 31));
        comboBox->setEditable(false);
        comboBox->setSizeAdjustPolicy(QComboBox::AdjustToContentsOnFirstShow);
        lineEdit = new QLineEdit(MenuWindow);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(50, 210, 181, 31));
        label = new QLabel(MenuWindow);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 220, 54, 20));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label_2 = new QLabel(MenuWindow);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 140, 54, 20));
        label_2->setFont(font);
        btnCreateRule = new QPushButton(MenuWindow);
        btnCreateRule->setObjectName(QString::fromUtf8("btnCreateRule"));
        btnCreateRule->setGeometry(QRect(50, 170, 101, 31));
        btnCreateRule->setAutoFillBackground(false);
        btnCreateRule->setStyleSheet(QString::fromUtf8(""));
        onoff = new OnOff(MenuWindow);
        onoff->setObjectName(QString::fromUtf8("onoff"));
        onoff->setGeometry(QRect(50, 290, 70, 36));
        onoff->setAcceptDrops(false);

        retranslateUi(MenuWindow);

        QMetaObject::connectSlotsByName(MenuWindow);
    } // setupUi

    void retranslateUi(QFrame *MenuWindow)
    {
        MenuWindow->setWindowTitle(QCoreApplication::translate("MenuWindow", "Frame", nullptr));
        pushButton_3->setText(QString());
        pushButton_4->setText(QString());
        pushButton_5->setText(QString());
        btnTack->setText(QString());
        comboBox->setItemText(0, QCoreApplication::translate("MenuWindow", "\344\270\255\346\226\207", nullptr));
        comboBox->setItemText(1, QCoreApplication::translate("MenuWindow", "\350\213\261\346\226\207", nullptr));

        lineEdit->setText(QCoreApplication::translate("MenuWindow", "192.168.1.2", nullptr));
        label->setText(QCoreApplication::translate("MenuWindow", "IP:", nullptr));
        label_2->setText(QCoreApplication::translate("MenuWindow", "\346\240\241\345\207\206", nullptr));
        btnCreateRule->setText(QCoreApplication::translate("MenuWindow", "\345\210\233\345\273\272\346\240\241\345\207\206\345\260\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MenuWindow: public Ui_MenuWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MENUWINDOW_H
