#include "menuwindow.h"
#include "ui_menuwindow.h"
#include "onoff.h"
#include "QMessageBox.h"

MenuWindow::MenuWindow(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::MenuWindow)
{
    ui->setupUi(this);

    ui->onoff->setOnoffStatus(OnOff::ON);
}

MenuWindow::~MenuWindow()
{
    delete ui;
}

void MenuWindow::ShowNullMsg()
{
    QString strTitle = "提示";
    QString strText =  "功能未实现";
    QMessageBox::information(this, strTitle, strText, QMessageBox::Ok, QMessageBox::NoButton);
}

void MenuWindow::on_btnCreateRule_clicked()
{
    ShowNullMsg();
}
