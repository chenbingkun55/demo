#include "OnOff.h"
#include <QPropertyAnimation>
#include <QPainter>
#include <QMouseEvent>
#include <QDebug>
#include <QTimer>
#include <QEventLoop>

OnOff::OnOff(QWidget *parent)
    : QWidget(parent)
    , m_onoffStatus(ON)
    , m_animationIsRun(false)
{
    m_propertyAnimation=new QPropertyAnimation(this,"minimumSize");
}

int OnOff::onoffStatus() const
{
    return m_onoffStatus;
}

void OnOff::setOnoffStatus(const int status)
{
    m_onoffStatus=status;

    m_animationIsRun=true;

    update();

    //Began to change
    controlWidgetChanged();
    m_animationIsRun=false;

    emit onoffStatusChanged(m_onoffStatus);
}

void OnOff::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter painter(this);
    painter.save();
    if(m_onoffStatus){
        painter.drawPixmap(0,0,QPixmap(":/images/icons/off.png").scaled(minimumSize()));
    }
    else{
        painter.drawPixmap(0,0,QPixmap(":/images/icons/on.png").scaled(minimumSize()));
    }
    painter.restore();
}

void OnOff::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton&&!m_animationIsRun){

        m_animationIsRun=true;
        bool onoffStatus=m_onoffStatus;
        setOnoffStatus(!onoffStatus);
    }
    event->accept();
}

void OnOff::controlWidgetChanged()
{
    m_propertyAnimation->setDuration(500);
    m_propertyAnimation->setStartValue(QSize(70,36));
    m_propertyAnimation->setEndValue(QSize(75,41));
    m_propertyAnimation->start();

    QEventLoop loop;
    QTimer::singleShot(1*600,&loop,&QEventLoop::quit);
    loop.exec();

    m_propertyAnimation->setDuration(500);
    m_propertyAnimation->setStartValue(QSize(75,41));
    m_propertyAnimation->setEndValue(QSize(70,36));
    m_propertyAnimation->start();

    QTimer::singleShot(1*500,&loop,&QEventLoop::quit);
    loop.exec();
}
