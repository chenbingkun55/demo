/********************************************************************************
** Form generated from reading UI file 'onoff.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ONOFF_H
#define UI_ONOFF_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OnOff
{
public:

    void setupUi(QWidget *OnOff)
    {
        if (OnOff->objectName().isEmpty())
            OnOff->setObjectName(QString::fromUtf8("OnOff"));
        OnOff->resize(70, 36);

        retranslateUi(OnOff);

        QMetaObject::connectSlotsByName(OnOff);
    } // setupUi

    void retranslateUi(QWidget *OnOff)
    {
        OnOff->setWindowTitle(QCoreApplication::translate("OnOff", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class OnOff: public Ui_OnOff {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ONOFF_H
