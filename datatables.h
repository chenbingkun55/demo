#ifndef DATATABLES_H
#define DATATABLES_H

#include <QWidget>
#include <QFrame>

namespace Ui {
class DataTables;
}

class DataTables : public QFrame
{
    Q_OBJECT

public:
    explicit DataTables(QWidget *parent = nullptr);
    ~DataTables();

private:
    Ui::DataTables *ui;
};

#endif // DATATABLES_H
