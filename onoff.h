#ifndef ONOFF_H
#define ONOFF_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QPropertyAnimation;
QT_END_NAMESPACE

class OnOff : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(int onoffStatus READ onoffStatus WRITE setOnoffStatus NOTIFY onoffStatusChanged)
public:
    enum status{ON,OFF};
    OnOff(QWidget *parent = 0);

    int onoffStatus()const;
    void setOnoffStatus(const int status);

signals:
    void onoffStatusChanged(int status);

protected:
    void paintEvent(QPaintEvent *event)Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event)Q_DECL_OVERRIDE;

    void controlWidgetChanged();

private:
    int m_onoffStatus;
    bool m_animationIsRun;
    QPropertyAnimation *m_propertyAnimation;
};

#endif // ONOFF_H
