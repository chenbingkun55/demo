/********************************************************************************
** Form generated from reading UI file 'datatables.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATATABLES_H
#define UI_DATATABLES_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DataTables
{
public:
    QTabWidget *tabWidget;
    QWidget *tab;
    QTableWidget *ce_liang_biao;
    QPushButton *pushButton;
    QWidget *tab_2;
    QPushButton *pushButton_2;
    QTableWidget *ce_liang_biao_2;

    void setupUi(QWidget *DataTables)
    {
        if (DataTables->objectName().isEmpty())
            DataTables->setObjectName(QString::fromUtf8("DataTables"));
        DataTables->resize(1280, 250);
        tabWidget = new QTabWidget(DataTables);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 1281, 250));
        tabWidget->setIconSize(QSize(16, 16));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        ce_liang_biao = new QTableWidget(tab);
        if (ce_liang_biao->columnCount() < 11)
            ce_liang_biao->setColumnCount(11);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        ce_liang_biao->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        ce_liang_biao->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        ce_liang_biao->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        ce_liang_biao->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        ce_liang_biao->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        ce_liang_biao->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        ce_liang_biao->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        ce_liang_biao->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        ce_liang_biao->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        ce_liang_biao->setHorizontalHeaderItem(9, __qtablewidgetitem9);
        if (ce_liang_biao->rowCount() < 6)
            ce_liang_biao->setRowCount(6);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        ce_liang_biao->setItem(0, 0, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        ce_liang_biao->setItem(0, 1, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        ce_liang_biao->setItem(1, 0, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        ce_liang_biao->setItem(2, 0, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        ce_liang_biao->setItem(5, 0, __qtablewidgetitem14);
        ce_liang_biao->setObjectName(QString::fromUtf8("ce_liang_biao"));
        ce_liang_biao->setGeometry(QRect(0, 40, 1271, 191));
        ce_liang_biao->setAlternatingRowColors(false);
        ce_liang_biao->setRowCount(6);
        ce_liang_biao->setColumnCount(11);
        ce_liang_biao->horizontalHeader()->setDefaultSectionSize(100);
        pushButton = new QPushButton(tab);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(10, 10, 75, 23));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        pushButton_2 = new QPushButton(tab_2);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(10, 10, 75, 23));
        ce_liang_biao_2 = new QTableWidget(tab_2);
        if (ce_liang_biao_2->columnCount() < 11)
            ce_liang_biao_2->setColumnCount(11);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        ce_liang_biao_2->setHorizontalHeaderItem(0, __qtablewidgetitem15);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        ce_liang_biao_2->setHorizontalHeaderItem(1, __qtablewidgetitem16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        ce_liang_biao_2->setHorizontalHeaderItem(2, __qtablewidgetitem17);
        QTableWidgetItem *__qtablewidgetitem18 = new QTableWidgetItem();
        ce_liang_biao_2->setHorizontalHeaderItem(3, __qtablewidgetitem18);
        QTableWidgetItem *__qtablewidgetitem19 = new QTableWidgetItem();
        ce_liang_biao_2->setHorizontalHeaderItem(4, __qtablewidgetitem19);
        QTableWidgetItem *__qtablewidgetitem20 = new QTableWidgetItem();
        ce_liang_biao_2->setHorizontalHeaderItem(5, __qtablewidgetitem20);
        QTableWidgetItem *__qtablewidgetitem21 = new QTableWidgetItem();
        ce_liang_biao_2->setHorizontalHeaderItem(6, __qtablewidgetitem21);
        QTableWidgetItem *__qtablewidgetitem22 = new QTableWidgetItem();
        ce_liang_biao_2->setHorizontalHeaderItem(7, __qtablewidgetitem22);
        QTableWidgetItem *__qtablewidgetitem23 = new QTableWidgetItem();
        ce_liang_biao_2->setHorizontalHeaderItem(8, __qtablewidgetitem23);
        QTableWidgetItem *__qtablewidgetitem24 = new QTableWidgetItem();
        ce_liang_biao_2->setHorizontalHeaderItem(9, __qtablewidgetitem24);
        if (ce_liang_biao_2->rowCount() < 6)
            ce_liang_biao_2->setRowCount(6);
        QTableWidgetItem *__qtablewidgetitem25 = new QTableWidgetItem();
        ce_liang_biao_2->setItem(0, 0, __qtablewidgetitem25);
        QTableWidgetItem *__qtablewidgetitem26 = new QTableWidgetItem();
        ce_liang_biao_2->setItem(0, 1, __qtablewidgetitem26);
        QTableWidgetItem *__qtablewidgetitem27 = new QTableWidgetItem();
        ce_liang_biao_2->setItem(1, 0, __qtablewidgetitem27);
        QTableWidgetItem *__qtablewidgetitem28 = new QTableWidgetItem();
        ce_liang_biao_2->setItem(2, 0, __qtablewidgetitem28);
        QTableWidgetItem *__qtablewidgetitem29 = new QTableWidgetItem();
        ce_liang_biao_2->setItem(5, 0, __qtablewidgetitem29);
        ce_liang_biao_2->setObjectName(QString::fromUtf8("ce_liang_biao_2"));
        ce_liang_biao_2->setGeometry(QRect(0, 40, 1024, 191));
        ce_liang_biao_2->setAlternatingRowColors(false);
        ce_liang_biao_2->setRowCount(6);
        ce_liang_biao_2->setColumnCount(11);
        ce_liang_biao_2->horizontalHeader()->setDefaultSectionSize(100);
        tabWidget->addTab(tab_2, QString());

        retranslateUi(DataTables);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(DataTables);
    } // setupUi

    void retranslateUi(QWidget *DataTables)
    {
        DataTables->setWindowTitle(QCoreApplication::translate("DataTables", "Form", nullptr));
        QTableWidgetItem *___qtablewidgetitem = ce_liang_biao->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("DataTables", "\345\272\217\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = ce_liang_biao->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("DataTables", "\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = ce_liang_biao->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("DataTables", "\344\275\215\347\275\256", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = ce_liang_biao->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("DataTables", "\351\225\277\345\272\246", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = ce_liang_biao->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("DataTables", "\345\256\275\345\272\246", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = ce_liang_biao->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("DataTables", "\351\253\230\345\272\246", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = ce_liang_biao->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("DataTables", "\345\221\250\351\225\277", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = ce_liang_biao->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("DataTables", "\351\235\242\347\247\257", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = ce_liang_biao->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QCoreApplication::translate("DataTables", "\345\215\212\345\276\204", nullptr));
        QTableWidgetItem *___qtablewidgetitem9 = ce_liang_biao->horizontalHeaderItem(9);
        ___qtablewidgetitem9->setText(QCoreApplication::translate("DataTables", "\350\247\222\345\272\246", nullptr));

        const bool __sortingEnabled = ce_liang_biao->isSortingEnabled();
        ce_liang_biao->setSortingEnabled(false);
        ce_liang_biao->setSortingEnabled(__sortingEnabled);

        pushButton->setText(QCoreApplication::translate("DataTables", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("DataTables", "\346\265\213\351\207\217\350\241\250", nullptr));
        pushButton_2->setText(QCoreApplication::translate("DataTables", "\345\210\240\351\231\244", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = ce_liang_biao_2->horizontalHeaderItem(0);
        ___qtablewidgetitem10->setText(QCoreApplication::translate("DataTables", "\345\272\217\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = ce_liang_biao_2->horizontalHeaderItem(1);
        ___qtablewidgetitem11->setText(QCoreApplication::translate("DataTables", "\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem12 = ce_liang_biao_2->horizontalHeaderItem(2);
        ___qtablewidgetitem12->setText(QCoreApplication::translate("DataTables", "\344\275\215\347\275\256", nullptr));
        QTableWidgetItem *___qtablewidgetitem13 = ce_liang_biao_2->horizontalHeaderItem(3);
        ___qtablewidgetitem13->setText(QCoreApplication::translate("DataTables", "\351\225\277\345\272\246", nullptr));
        QTableWidgetItem *___qtablewidgetitem14 = ce_liang_biao_2->horizontalHeaderItem(4);
        ___qtablewidgetitem14->setText(QCoreApplication::translate("DataTables", "\345\256\275\345\272\246", nullptr));
        QTableWidgetItem *___qtablewidgetitem15 = ce_liang_biao_2->horizontalHeaderItem(5);
        ___qtablewidgetitem15->setText(QCoreApplication::translate("DataTables", "\351\253\230\345\272\246", nullptr));
        QTableWidgetItem *___qtablewidgetitem16 = ce_liang_biao_2->horizontalHeaderItem(6);
        ___qtablewidgetitem16->setText(QCoreApplication::translate("DataTables", "\345\221\250\351\225\277", nullptr));
        QTableWidgetItem *___qtablewidgetitem17 = ce_liang_biao_2->horizontalHeaderItem(7);
        ___qtablewidgetitem17->setText(QCoreApplication::translate("DataTables", "\351\235\242\347\247\257", nullptr));
        QTableWidgetItem *___qtablewidgetitem18 = ce_liang_biao_2->horizontalHeaderItem(8);
        ___qtablewidgetitem18->setText(QCoreApplication::translate("DataTables", "\345\215\212\345\276\204", nullptr));
        QTableWidgetItem *___qtablewidgetitem19 = ce_liang_biao_2->horizontalHeaderItem(9);
        ___qtablewidgetitem19->setText(QCoreApplication::translate("DataTables", "\350\247\222\345\272\246", nullptr));

        const bool __sortingEnabled1 = ce_liang_biao_2->isSortingEnabled();
        ce_liang_biao_2->setSortingEnabled(false);
        ce_liang_biao_2->setSortingEnabled(__sortingEnabled1);

        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("DataTables", "\350\256\241\346\225\260\350\241\250", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DataTables: public Ui_DataTables {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATATABLES_H
