#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "toptools.h"
#include "datatables.h"
#include "menuwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    auto topTools = new TopTools();
    ui->top->addWidget(topTools);

    auto dataTables = new DataTables();
    ui->down->addWidget(dataTables);

    auto menuwindow = new MenuWindow();
    ui->left->addWidget(menuwindow);
}

MainWindow::~MainWindow()
{
    delete ui;
}
